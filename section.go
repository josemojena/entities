package entities

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

//LangField struct
type LangField struct {
	Field []map[string]string
}

//Section struct
type Section struct {
	ID          bson.ObjectId     `json:"id" bson:"_id,omitempty"`
	Name        map[string]string `json:"name" binding:"required"`
	Description map[string]string `json:"description" binding:"required"`
	Slug        map[string]string `json:"slug"`
	Rank        int               `json:"rank"`
	Icon        string            `json:"icon"`
	CSS         string            `json:"css"`
	NumberSites int               `json:"numberSites"`
	Active      string            `json:"active"`
	SeoModule   SeoModule         `json:"seoModule"`
	UpdatedAt   time.Time         `json:"updated_at"`
	CreatedAt   time.Time         `json:"created_at"`
}

//Fake function that implements the IEntity interface
func (s Section) Fake() {
	//Nothing to do
}

//Bind section model
func (s *Section) Bind(sparam IEntity) {
	sBinded := sparam.(Section)
	date := time.Now()
	s.ID = sBinded.ID
	s.Name = sBinded.Name
	s.Description = sBinded.Description

	if s.Description["es"] == "" || s.Description["en"] == "" {
		panic("Description empty not allowed")
	}

	s.Description = sBinded.Description
	s.Rank = sBinded.Rank
	s.Icon = sBinded.Icon
	s.Slug = sBinded.Slug
	s.CSS = sBinded.CSS
	s.NumberSites = sBinded.NumberSites
	s.Active = sBinded.Active
	s.SeoModule = sBinded.SeoModule
	//Only if it is new
	if len(s.ID) == 0 {
		s.CreatedAt = date
	}
	s.UpdatedAt = date
}

//SetUpdatedAt update the last modification time
func (s *Section) SetUpdatedAt() {
	date := time.Now() //.Format("2006-01-02 15:04:05")
	s.UpdatedAt = date
}
