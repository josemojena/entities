package entities

//Photo struct
type Photo struct {
	Name               string `json:"name" binding:"required"`
	Path               string `json:"path" binding:"required"`
	ProgramUsed        string `json:"programUsed" binding:"required"`
	PhotoCookie        string `json:"photoCookie"`
	PhotoUpdatedAt     string `json:"photoUpdatedAt"`
	UpdateInBackground bool   `json:"updateInBackground"`
}
