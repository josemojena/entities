package entities

//SeoModule struct
type SeoModule struct {
	SectionName     map[string]string `json:"sectionName"`
	PageTitle       map[string]string `json:"pageTitle"`
	PageDescription map[string]string `json:"pageDescription"`
}
