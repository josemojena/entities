package entities

import "gopkg.in/mgo.v2/bson"

//DiscountPrice handle the prices
type DiscountPrice struct {
	Percent string            `json:"percent"`
	Label   string            `json:"label"`
	Fields  map[string]string `json:"fields"`
	Active  bool              `json:"active"`
}

//Discount handle the offers
type Discount struct {
	ID               bson.ObjectId     `json:"id" bson:"_id"`
	Title            map[string]string `json:"title"`
	ShortDescription map[string]string `json:"short_description"`
	LongDescription  map[string]string `json:"long_description"`
	Thumbnail        string            `json:"thumbnail"`
	Active           bool              `json:"active"`
	Prices           DiscountPrice     `json:"prices"`
	ExpireAt         string            `json:"expire_at"`
	SiteId           bson.ObjectId     `json:"site_id" bson:"site_id"`
	OldPrice         string            `json:old_price`
	NewPrice         string            `json:new_price`
	ExtraFields      map[string]string `json:extra_fields`
}
//Fake function that implements the IEntity interface
func (discount Discount) Fake() {
	//Nothing to do
}