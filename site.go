package entities

import (
	"math"
	"strings"
	"time"

	"github.com/gosimple/slug"
	"gopkg.in/mgo.v2/bson"
)

//ScreenShotConfig struct handle delay time to take a remote photo for a site
type ScreenShotConfig struct {
	Delay   int `json:"delay"`
	MaxWait int `json:"maxWait"`
}

//SubLink not sure if this  is used
type SubLink struct {
	Link    string `json:"Link"`
	Text    string `json:"Text"`
	ID      string `json:"Id"`
	Section string `json:"Sections"`
	Rank    Rank   `json:"Rank"`
}

//Store the affiliate meta
type Affiliate struct {
	IsAffiliate   bool   `json:"isAffiliate"`
	AffiliateURL  string `json:"affiliateUrl"`
	AffiliateMeta bson.M `json:"affiliateMeta"`
}

//Rank struct store different ranks classifications
type Rank struct {
	PornFinger float64 `json:"pornfingerRank"`
	AlexaRank  float64 `json:"alexaRank"`
	UserRank   float64 `json:"userRank"`
	RankTotal  float64 `json:"rankTotal"`
}
type MetaInformation struct {
	MetaTags        string `json:"metaTags"`
	MetaDescription string `json:"metaDescription"`
	MetaTitle       string `json:"metaTitle"`
	MetaContent     string `json:"metaContent"`
}

//UserInteractions handle total user's statics for every site
type UserInteractions struct {
	Reviews  float64 `json:"userReview"`
	Views    int     `json:"views"`
	Likes    int     `json:"likes"`
	Dislikes int     `json:"dislikes"`
	Comments int     `json:"comments"`
}

//BodyForRequest that catch the body site request and bind it with Site struct
type BodyForRequest struct {
	ID                     bson.ObjectId     `json:"id" bson:"_id"`
	Name                   string            `json:"name" binding:"required"`
	URL                    string            `json:"url"`
	MetaTags               string            `json:"metaTags"`
	Content                map[string]string `json:"content"`
	Rank                   int               `json:"rank"`
	Section                bson.ObjectId     `json:"section" bson:"section_id"`
	PhotoPath              string            `json:"photoPath"`
	UpdateInBackground     bool              `json:"updateInBackground"`
	Delay                  int               `json:"delay"`
	MaxWait                int               `json:"maxWait"`
	ParentSite             bson.ObjectId     `json:"parentSite" bson:"parentSite"`
	IsParent               int               `json:"isParent"`
	UpdateMetaInBackground bool              `json:"updateMetaInBackground"`
	Status                 bool              `json:"status"`
	PhotoSelection         string            `json:"photoSelection"`
	Photocookie            string            `json:"photoCookie"`
	Affiliate              Affiliate         `json:"affiliate"`
	SeoModule              SeoModule         `json:"seo"`
}

//Site struct represent a site model
//Fix that name of search is the name of the parent
type Site struct {
	Id                     bson.ObjectId     `json:"_id" bson:"_id,omitempty"`
	Name                   string            `json:"name"`
	NameForSearch          string            `json:"nameForSearch" bson:"nameOfSearch,omitempty"`
	Slug                   string            `json:"slug"`
	Url                    string            `json:"url"`
	ParentSite             bson.ObjectId     `json:"parentSite" bson:"parentSite,omitempty"`
	Content                map[string]string `json:"content"`
	MetaInformation        MetaInformation   `json:"metaInformation"`
	Section                bson.ObjectId     `json:"section" bson:"section_id"`
	Photos                 [2]Photo          `json:"photos"`
	ScreenConfig           ScreenShotConfig  `json:"screenConfig"`
	UpdateMetaInBackground bool              `json:"updateMetaInBackground"`
	Rank                   Rank              `json:"pornfingerRank"`
	IsParent               int               `json:"isParent" bson:"isParent"`
	UserInteractions       UserInteractions  `json:"userInteractions"`
	CreatedAt              string            `json:"createdAt"`
	UpdatedAt              string            `json:"updatedAt"`
	Status                 bool              `json:"status"`
	ReIndexed              bool              `json:"reIndexed"`
	Affiliate              Affiliate         `json:"affiliate"`
	IconDownloaded         bool              `json:"iconDownloaded"`
	SeoModule              SeoModule         `json:"seoModule"`
	Files                  []bson.ObjectId   `json:files`
}

//Fake function that implements the IEntity interface
func (s Site) Fake() {
	//Nothing to do
}

//Bind request values to the Site Model
func (s *Site) Bind(request BodyForRequest) {

	date := time.Now().Format("2006-01-02 15:04:05")

	pfRank := float64(request.Rank)

	if len(request.ID) > 0 {
		//the rank depends of whether the site is new
		s.Id = request.ID
		s.Rank.PornFinger = pfRank
	} else {
		//New site
		s.Id = bson.NewObjectId()
		s.CreatedAt = date
		rank := Rank{
			PornFinger: pfRank,
			AlexaRank:  math.MaxInt32,
			UserRank:   math.MaxInt32,
			RankTotal:  0,
		}
		s.Rank = rank
	}

	s.UpdatedAt = date
	s.Name = strings.ToLower(request.Name)
	s.Slug = slug.Make(s.Name)
	s.Url = request.URL

	if len(request.ParentSite) > 0 {
		s.ParentSite = request.ParentSite
		s.IsParent = 0
	} else {
		s.ParentSite = s.Id
		s.IsParent = 1

	}
	//all field repeat the name of the parent
	s.SetNameOfSearch(request.Name)

	metaInfo := MetaInformation{
		MetaTags:        request.MetaTags,
		MetaTitle:       "",
		MetaContent:     "",
		MetaDescription: "",
	}
	s.MetaInformation = metaInfo

	s.Content = request.Content
	//Site rank

	s.Section = request.Section
	s.UpdateMetaInBackground = request.UpdateMetaInBackground
	//this data will be update by the frontend' user

	s.UserInteractions.Reviews = 0
	s.UserInteractions.Views = 0

	s.Status = request.Status
	s.ReIndexed = false

	//Affiliate information
	s.Affiliate = request.Affiliate
	//indicate if a site has an icon
	s.IconDownloaded = false

	s.SeoModule = request.SeoModule
}

//MakeSlug update the site's slug
func (s *Site) MakeSlug(val string) {
	s.Slug = slug.Make(val)
}

//SetParent function
func (s *Site) SetParent(isParent int) {
	s.IsParent = isParent
}

//SetRank function
func (s *Site) SetRank(rank float64) {
	s.Rank.PornFinger = rank
}

//SetPhotos function
func (s *Site) SetPhotos(photos [2]Photo) {
	s.Photos = photos
}

//SetNameOfSearch function, this name will be consulted by elastic search engine
func (s *Site) SetNameOfSearch(nameOfSearch string) {
	s.NameForSearch = nameOfSearch
}

//SetMetaData function
func (s *Site) SetMetaData(data map[string]string) {

	//Default content
	/*s.MetaInformation.MetaTitle = ""
	s.MetaInformation.MetaContent = ""
	s.MetaInformation.MetaDescription = ""*/

	//update from remote scrapping
	if data["metaTitle"] != "" {
		s.MetaInformation.MetaTitle = data["metaTitle"]
	}
	if data["metaContent"] != "" {
		s.MetaInformation.MetaContent = data["metaContent"]
	}
	if data["metaDescription"] != "" {
		s.MetaInformation.MetaDescription = data["metaDescription"]
	}
	if s.UpdateMetaInBackground == true && len(data["metaTags"]) > 0 {
		s.MetaInformation.MetaTags = data["metaTags"]
	}
}

//Add files
func (s *Site) AddFile(id bson.ObjectId) {
	s.Files = append(s.Files, id)
}

//Remove a file by id
func (s *Site) RemoveFile(id bson.ObjectId) {

	var tempFiles = make([]bson.ObjectId, len(s.Files)-1)
	for _, file := range s.Files {

		if file != id {
			tempFiles = append(tempFiles, file)
		}
	}
	s.Files = tempFiles
}
