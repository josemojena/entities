package entities

import "gopkg.in/mgo.v2/bson"

//DeletedFiles handle the files that will be removed
type DeletedFiles struct {
	Path      string        `json:"path"`
	ID        bson.ObjectId `json:"id" bson:"id,omitempty"`
	Directory string        `json:"directory"`
}

//Fake method that implement the IEntity interface
func (d DeletedFiles) Fake() {
	//nothing to do here
}

//Bind match the file and the root path to make a valid path
func (d *DeletedFiles) Bind(path string, rootPath string) {
	d.Path = path
	d.ID = bson.NewObjectId()
	d.Directory = rootPath
}
