package entities

import "gopkg.in/mgo.v2/bson"

//Configuration struct to handle some configuration values link(useCache, alexRank )among the other configs
type Configuration struct {
	ID    bson.ObjectId `json:"_id" bson:"_id,omitempty"`
	Name  string        `json:"name"`
	Value string        `json:"value"`
}

//Fake implements the interface IEntity
func (c Configuration) Fake() {
	//fake function
}
