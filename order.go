package entities

import "gopkg.in/mgo.v2/bson"

//Order struct keep track of the rank of each site
type Order struct {
	ID      bson.ObjectId   `json:"id" bson:"_id,omitempty"`
	Section bson.ObjectId   `json:"section" bson:"section_id"`
	Sites   []bson.ObjectId `json:"sites"`
}

//Fake implements the interface IEntity
func (o Order) Fake() {
	//fake function
}
