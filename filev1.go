package entities

import "gopkg.in/mgo.v2/bson"

//Handle the list of images for each site
type FileV1 struct {
	Id     bson.ObjectId `json:"_id" bson:"_id,omitempty"`
	Medium string        `json:"medium"`
	Small  string        `json:"small"`
	SiteID bson.ObjectId `json:siteId`
}

//Fake function that implements the IEntity interface
func (f FileV1) Fake() {
	//Nothing to do
}

func (f *FileV1) Bind(small, medium string, siteID bson.ObjectId) {

	f.Id = bson.NewObjectId()
	f.Small = small
	f.Medium = medium
	f.SiteID = siteID
}
