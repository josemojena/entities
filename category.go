package entities

import "gopkg.in/mgo.v2/bson"

//Category struct
type Category struct {
	Id    bson.ObjectId `json:"id" bson:"id,omitempty"`
	Name  string        `json:"name" binding:"required"`
	Value string        `json:"value" binding:"required"`
}
